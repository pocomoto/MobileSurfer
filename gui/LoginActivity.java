package com.example.itoma.test001;

import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.View;
import android.widget.EditText;

import com.example.itoma.test001.database.DatabaseHandler;
import com.example.itoma.test001.database.model.User;

public class LoginActivity extends AppCompatActivity {
    EditText mEmail, mPass;
    DatabaseHandler mHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        mEmail = (EditText) findViewById(R.id.login_email);
        mPass = (EditText) findViewById(R.id.login_pass);

        mHandler = new DatabaseHandler(this);
    }

    public void doLogin(View view) {
        String email = mEmail.getText().toString();
        String pass = mPass.getText().toString();

        if (!isValidEmail(email)) {
            mEmail.setError("Email not valid!");
        } else if (!isValidPassword(pass)) {
            mPass.setError("Password not valid!");
        } else {
            User user = User.getByEmailAndPass(mHandler, email, pass);
            if (user == null) {
                Snackbar.make(view, "Cannot Find User", Snackbar.LENGTH_LONG).show();
            } else {
                startActivity(new Intent(getBaseContext(), MainActivity.class));
                finish();
            }
        }
    }

    public boolean isValidEmail(String email) {
        return !email.isEmpty() && Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    public boolean isValidPassword(String pass) {
        return !pass.isEmpty() && pass.length() >= 6;
    }

    public void doRegister(View view) {
        startActivity(new Intent(getBaseContext(), RegisterActivity.class));
    }
}
