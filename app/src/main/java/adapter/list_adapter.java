//package adapter;
//
///**
// * Created by user on 29/06/2015.
// */
//import android.content.Context;
//import android.graphics.drawable.Drawable;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.BaseAdapter;
//import android.widget.Button;
//import android.widget.ImageView;
//import android.widget.TextView;
//
//import com.tas.hungryme.BasketActivity;
//import com.tas.hungryme.R;
//
//import java.util.ArrayList;
//
//import model.OrderHelper;
//
//
//public class list_adapter extends BaseAdapter {
//
//    Context context;
//    LayoutInflater layoutInflater;
//    Integer jml;
//    Integer hrg;
//    public static Integer t;
//    public static Integer a = View.INVISIBLE;
//
//    public list_adapter(Context context) {
//        this.context = context;
//        this.layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//        if(a == View.VISIBLE)
//            BasketActivity.edit.setText("Finish");
//    }
//
//    @Override
//    public int getCount() {
//        return OrderHelper.getOrder().size();
//    }
//
//    @Override
//    public Object getItem(int position) {
//        return OrderHelper.getOrder().get(position);
//    }
//
//    @Override
//    public long getItemId(int position) {
//        return position;
//    }
//
//    @Override
//    public View getView(final int position, View convertView, ViewGroup parent) {
//        View view = layoutInflater.inflate(R.layout.list_nama_row_layout,null);
//
//
//
//        TextView textView = (TextView) view.findViewById(R.id.textView);
//        final TextView qty = (TextView) view.findViewById(R.id.textView5);
//        final TextView tot = (TextView) view.findViewById(R.id.textView6);
//        textView.setText(OrderHelper.getOrder().get(position).getMenu_name());
//        jml=OrderHelper.getOrder().get(position).getMenu_qty();
//        hrg=OrderHelper.getOrder().get(position).getMenu_total_price();
//        qty.setText(jml+"");
//        tot.setText(hrg*jml+"");
//
//        final Button plus=(Button) view.findViewById(R.id.plus);
//        final Button min=(Button) view.findViewById(R.id.minus);
//
//        plus.setVisibility(a);
//        min.setVisibility(a);
//
//        plus.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                hrg=OrderHelper.getOrder().get(position).getMenu_total_price();
//                jml = Integer.parseInt(qty.getText().toString());
//                if (jml != 10) {
//                    t = t - OrderHelper.getOrder().get(position).getMenu_qty() * hrg;
//                    OrderHelper.getOrder().get(position).setMenu_qty(OrderHelper.getOrder().get(position).getMenu_qty()+1);;
//                    qty.setText(OrderHelper.getOrder().get(position).getMenu_qty() + "");
//                    tot.setText(OrderHelper.getOrder().get(position).getMenu_qty() * hrg + "");
//                    t = t + OrderHelper.getOrder().get(position).getMenu_qty() * hrg;
//                    BasketActivity.jum.setText("" + t);
//
//                }
//            }
//        });
//
//        min.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                hrg=OrderHelper.getOrder().get(position).getMenu_total_price();
//                jml = Integer.parseInt(qty.getText().toString());
//                if (jml != 0) {
//                    t = t - OrderHelper.getOrder().get(position).getMenu_qty() * hrg;
//                    OrderHelper.getOrder().get(position).setMenu_qty(OrderHelper.getOrder().get(position).getMenu_qty()-1);
//                    qty.setText(OrderHelper.getOrder().get(position).getMenu_qty() + "");
//                    tot.setText(OrderHelper.getOrder().get(position).getMenu_qty() * hrg + "");
//                    t = t + OrderHelper.getOrder().get(position).getMenu_qty() * hrg;
//                    BasketActivity.jum.setText("" + t);
//                }
//            }
//        });
//        return view;
//    }
//}
