package model;

/**
 * Created by user on 29/06/2015.
 */
public class Hotels {
    private String nama;
    private String harga;
    private String gambar;

    public Hotels() {
    }

    public Hotels(String nama,String harga,String gambar) {
        super();
        this.nama = nama;
        this.harga = harga;
        this.gambar=gambar;

    }

    public String getNama() {
        return nama;
    }

    public void setNama(String title) {
        this.nama = nama;
    }

    public void setHarga(String title) {
        this.harga = harga;
    }

    public String getHarga() {
        return harga;
    }


    public String getGambar() {
        return gambar;
    }

    public void setGambar(String gambar) {
        this.gambar = gambar;
    }


}

